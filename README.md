Formulär för registrering av data till databas
========
## Krav

* PHP 5.5.3 **Utvecklat på php 5.5.3 och är ej testat på andra versioner**

## Installation

Skapa databas:

```sql
    DROP TABLE IF EXISTS `participants`;
    CREATE TABLE `participants` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `firstname` varchar(25) NOT NULL DEFAULT '',
      `lastname` varchar(40) NOT NULL DEFAULT '',
      `email` varchar(255) NOT NULL DEFAULT '',
      `company` varchar(150) NOT NULL DEFAULT '',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```


1. Öppna ```src/Services/ConnectionService.php``` och lägg till dina db-inställningar i ```$connections``` arrayen.
2. Öppna ```src/Services/Validator.php``` ställ in dina valideringsregler för varje fält.
3. Starta en lokal server i dokumentrooten

**Förinställda valideringsregler**
```php
"firstname"   => "required|min:3|max:25",
"lastname"    => "required|min:3|max:25",
"companyname" => "required|min:3|max:25",
"email"       => "required|email",
"terms"       => "accepted"
```


