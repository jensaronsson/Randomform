<?php  namespace Acme\Controllers;
use Acme\RegisterService\RegisterService;
use Acme\Views\Form;

class RegisterController
{
    private $view;
    private $service;

    /**
     * @param Form            $view
     * @param RegisterService $service
     */
    public function __construct(Form $view, RegisterService $service)
    {
        $this->view = $view;
        $this->service = $service;
    }

    public function run()
    {
        if($this->view->add())
        {
           $this->service->createAndSaveNewParticipant($this->view->getInput());
        }

       echo $this->view->getHtml();
    }

} 
