<?php  namespace Acme\Facades;
session_start();
require "src/Views/Form.php";
require "src/Controllers/RegisterController.php";
require "src/Services/RegisterService.php";
require "src/Services/NonceMaker.php";
require "src/Services/MessageHandler.php";
require "src/Services/Validator.php";
require "src/Services/CSRFToken.php";
require "src/Repositories/MysqlParticipantRepository.php";

use Acme\Controllers\RegisterController;
use Acme\RegisterService\RegisterService;
use Acme\Repositories\MysqlParticipantRepository;
use Acme\Services\CSRFToken;
use Acme\Services\MessageHandler;
use Acme\Services\NonceMaker;
use Acme\Services\Validator;
use Acme\Views\Form;

class Application
{
    public static function boot()
    {
        $errorHandler = new MessageHandler();
        $token = new CSRFToken(new NonceMaker);
        $service = new RegisterService(new MysqlParticipantRepository(), new Validator($errorHandler), $token);
        $view = new Form($service, $errorHandler, $token);
        $controller = new RegisterController($view, $service);
        $controller->run();
    }
}
