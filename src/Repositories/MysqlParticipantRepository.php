<?php  namespace Acme\Repositories;

require "ParticipantRepositoryInterface.php";
require "src/Services/ConnectionService.php";
use Acme\Models\Participant;
use Acme\Services\ConnectionService;

class MysqlParticipantRepository implements ParticipantRepositoryInterface
{

    public function __construct()
    {
        $this->conn = (new ConnectionService())->getMysql();
    }

    public function all()
    {
       $this->conn->getAllRecords("participants");
    }

    public function getTotalRows()
    {
        return $this->conn->getTotalRows("participants");
    }

    public function find($id)
    {
        $this->conn->getById($id);
    }

    public function add(Participant $participant)
    {
        $this->conn->persist("participants", [":firstname" =>$participant->getFirstname(),
                                            ":lastname" => $participant->getLastname(),
                                            ":company" =>$participant->getCompanyName(),
                                            ":email" => $participant->getEmailAddress()]);
    }

    public function getByEmail($email)
    {
        return $this->conn->getByField("participants", "email", $email);

    }


}
