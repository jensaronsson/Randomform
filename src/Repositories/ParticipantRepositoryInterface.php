<?php
namespace Acme\Repositories;

use Acme\Models\Participant;

interface ParticipantRepositoryInterface
{
    public function all();

    public function getTotalRows();

    public function find($id);

    public function add(Participant $participant);

    public function getByEmail($email);
}
