<?php  namespace Acme\Views;

use Acme\RegisterService\RegisterService;
use Acme\Services\CSRFToken;
use Acme\Services\MessageHandler;

class Form
{
    private $service;
    private $errorHandler;
    private $token;

    public function __construct(RegisterService $service,
                                MessageHandler $errorHandler,
                                CSRFToken $token)
    {
        $this->service = $service;
        $this->errorHandler = $errorHandler;
        $this->token = $token;
    }

    public function add()
    {
        return isset($_POST['register']);
    }

    public function getInput()
    {
        return $_POST;
    }

    public function getHtml()
    {
        $html = "
        <!doctype html>
        <html lang=\"sv\">
        <head>
        <meta charset=\"UTF-8\">
        <title>Register Participant</title>
        <link rel=\"stylesheet\" href=\"assets/style.css\"/>

        </head>
        <body>
        <div class=\"heading\">

        </div>

           <div class=\"registerform\">
           <h5 class=\"registerd\"> Registered: " . $this->service->getTotalRegistered() . "</h3>

              <h2 class=\"text-center form-heading\">Register Here</h2>

                       <span class=\"error\">" . $this->errorHandler->get('error.common') . "</span>
                    <form action=\"?register\" method=\"POST\">
                       <input type=\"hidden\" name=\"csrftoken\" value=\"" . $this->token->generateToken() . "\"/>

                       <div class=\"form-g\">
                           <div class=\"form-f\">
                               <span class=\"error\">" . $this->errorHandler->get('firstname') . "</span>
                               <label for=\"firstname\">Firstname:</label>
                               <input type=\"text\" name=\"firstname\" value=\"" . $this->getInputData('firstname') . "\"/>
                           </div>
                            <div class=\"form-f\">
                               <span class=\"error\">" . $this->errorHandler->get('lastname') . "</span>
                               <label for=\"lastname\">Lastname:</label>
                               <input type=\"text\" name=\"lastname\" value=\"" . $this->getInputData('lastname') . "\"/>
                           </div>
                       </div>

                        <div class=\"form-g\">
                            <div class=\"form-f\">
                               <span class=\"error\">" . $this->errorHandler->get('companyname') . "</span>
                               <label for=\"companyname\">Companyname:</label>
                               <input type=\"text\" name=\"companyname\" value=\"" . $this->getInputData('companyname') . "\"/>
                           </div>
                           <div class=\"form-f\">
                               <span class=\"error\">" . $this->errorHandler->get('email') . "</span>
                               <label for=\"email\">Email:</label>
                               <input type=\"text\" name=\"email\" value=\"" . $this->getInputData('email') . "\"/>
                           </div>
                        </div>
                       <span class=\"error\">" . $this->errorHandler->get('terms') . "</span>
                       <label for=\"terms\">
                       <input type=\"hidden\" name=\"terms\" value=\"0\" />
                       <input type=\"checkbox\" name=\"terms\" value=\"1\" /> Accept our terms & conditions
                       </label>
                       <input class=\"button\" type=\"submit\" value=\"Register\" name=\"register\"/>
                    </form></div>
        </body>
        </html>
";
        return $html;
    }

    public function getInputData($key, $default = "")
    {
        if (isset($_POST[$key])) {
            return $_POST[$key];
        }
        return $default;
    }
} 
