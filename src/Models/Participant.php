<?php  namespace Acme\Models;

class Participant
{

    private $firstName;
    private $lastName;
    private $companyName;
    private $emailAddress;

    public function __construct($firstName, $lastName, $companyName, $emailAddress)
    {
        foreach (get_defined_vars() as $param) {
            $this->guardType($param);
        }
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->companyName = $companyName;
        $this->emailAddress = $emailAddress;
    }

    private function guardType($firstName)
    {
        if (!is_string($firstName)) {
            throw new \Exception("Wrong format, string is excepted");
        }
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }


} 
