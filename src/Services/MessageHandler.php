<?php  namespace Acme\Services;

class MessageHandler
{
    private $messages;

    public function add($field, $message)
    {
        $this->messages[$field][] = $message;
    }

    public function get($field)
    {
        if ($this->has($field))
        {
            return end($this->messages[$field]);
        }
    }

    public function has($key)
    {
        return isset($this->messages[$key]);
    }

    public function all()
    {
        return $this->messages;
    }

    public function isEmpty()
    {
        return empty($this->messages);
    }

    public function remove($attribute)
    {
        if ($this->has($attribute)) {
            unset($this->messages[$attribute]);
        }
    }
}

