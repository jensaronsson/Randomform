<?php  namespace Acme\Services;

use Exception;

/**
 * Validator class
 * Specifying rules
 * TODO: Extract to baseclass, have each concrete class extend with own set of rules.
 */
class Validator
{

    private $rules = [
        "firstname"   => "required|min:3|max:25",
        "lastname"    => "required|min:3|max:25",
        "companyname" => "required|min:3|max:25",
        "email"       => "required|email",
        "terms"       => "accepted"
    ];

    private $errorHandler;

    public function __construct(MessageHandler $errorHandler)
    {
        $this->errorHandler = $errorHandler;
    }

    /**
     * Validates input data with given rules
     * @param array $input
     * @throws \Exception
     */
    public function validate(array $input)
    {
        foreach ($input as $attribute => $value) {
            // If the attribute has a matching validation rule
            if ($this->checkIfRuleIsValid($attribute)) {
                $this->run($this->rules[$attribute], $attribute, $value);
            }
        }
        if(!$this->errorHandler->isEmpty())
        {
            throw new Exception("There were some validation issues:");
        }
    }

    /**
     * Validate required fields
     *
     * @param $attribute
     * @param $input
     */
    public function validateRequired($attribute, $input)
    {
        if(empty($input))
        {
            $this->errorHandler->add($attribute, ucfirst($attribute) . " is required");
        }
    }

    /**
     * Validate if something is true
     * @param $attribute
     * @param $input
     */
    public function validateAccepted($attribute, $input)
    {
        if(!(bool)$input)
        {
            $this->errorHandler->add($attribute, "You need to accept our conditions");
        }
    }

    /**
     * Validate minimum characters
     * @param $attribute
     * @param $input
     * @param $limit
     */
    public function validateMin($attribute, $input, $limit)
    {
        if(strlen($input) < $limit)
        {
            $this->errorHandler->add($attribute, ucfirst($attribute) . " needs to be atleast $limit characters");
        }
    }

    /**
     * Validate maximum characters
     *
     * @param $attribute
     * @param $input
     * @param $limit
     */
    public function validateMax($attribute, $input, $limit)
    {
        if(strlen($input) > $limit)
        {
            $this->errorHandler->add($attribute, ucfirst($attribute) . " maximum of characters is $limit");
        }
    }

    /**
     * Validate emailaddress using php's builint validation filters
     *
     * @param $attribute
     * @param $input
     */
    public function validateEmail($attribute, $input)
    {
        if (!filter_var($input, FILTER_VALIDATE_EMAIL)) {
            $this->errorHandler->add($attribute, "Enter a valid emailaddress");
        }

    }

    /**
     * If a rule has a limit set to it. Let's extract.
     *
     * @param $value
     * @return array
     */
    private function extractLimits($value)
    {
        return explode(":", $value);
    }

    /**
     * Check if input data contains same keys as given rules array
     *
     * @param $attribute
     * @return bool
     */
    private function checkIfRuleIsValid($attribute)
    {
        return in_array($attribute, array_keys($this->rules));
    }

    /**
     * Now that we now which data to validate against what rule.
     * Lets start calling the validation methods
     *
     * @param $ruleattr
     * @param $attribute
     * @param $value
     */
    private function run($ruleattr, $attribute, $value)
    {
        $rules = explode('|', $ruleattr);
        // Sorts rules lexographic so that required is run last.
        sort($rules);
        foreach ($rules as $rule) {
            $rule = ucfirst($rule);
            if ($this->hasLimit($rule)) {
                list($rule, $limit) = $this->extractLimits($rule);
                call_user_func_array([$this, "validate{$rule}"], [$attribute, $value, $limit]);
            } else {
                call_user_func_array([$this, "validate{$rule}"], [$attribute, $value]);
            }
        }
    }

    /**
     * Check is rule has a limit set
     *
     * @param $value
     * @return bool
     */
    private function hasLimit($value)
    {
        return (bool)preg_match("/(max|min)/i", $value);
    }

    /**
     * Add a validation message to errorbag
     *
     * @param $field
     * @param $message
     */
    public function add($field, $message)
    {
        $this->errorHandler->add($field,$message);
    }

    public function isValid()
    {
        return $this->errorHandler->isEmpty();
    }
}
