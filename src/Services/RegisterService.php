<?php  namespace Acme\RegisterService; 
require "src/Models/Participant.php";
use Acme\Models\Participant;
use Acme\Repositories\MysqlParticipantRepository;
use Acme\Services\CSRFToken;
use Acme\Services\Validator;
use Exception;

class RegisterService
{

    /**
     * @var ErrorBag $errorBag
     */
    private $repo;

    /**
     * @var MysqlParticipantRepository $repo
     */
    private $errorBag;
    /**
     *
     */
    private $validator;
    /**
     *
     */
    private $token;

    public function __construct(MysqlParticipantRepository $repo, Validator $validator, CSRFToken $token)
    {
        $this->repo = $repo;
        $this->validator = $validator;
        $this->token = $token;
    }

    public function createAndSaveNewParticipant(array $input)
    {
        try {
            $this->validateCsrfToken($input["csrftoken"]);
            $this->validator->validate($input);
            $participant = new Participant($input['firstname'], $input['lastname'], $input['companyname'], $input['email']);
            if ($this->repo->getByEmail($participant->getEmailAddress())) {
                throw new Exception("Someone has already registered this email address");
            }
            $this->repo->add($participant);
            return $participant;
        } catch (Exception $e)
        {
            $this->validator->add("error.common", $e->getMessage());
        }
    }

    public function getParticipantRepo()
    {
        return $this->repo;
    }

    public function getTotalRegistered()
    {
            return $this->repo->getTotalRows();
    }

    public function validateCsrfToken($token)
    {
       $this->token->validateToken($token);
    }


}
