<?php  namespace Acme\Services;
require "MysqlConnector.php";

class ConnectionService
{
    private static $connections = [
        "mysql" => [
            "dsn" => "mysql:host=127.0.0.1;dbname=Participants",
            "username" => "root",
            "password" => ""
        ]
    ];
    public function getMysql()
    {
        return new MysqlConnector(self::$connections["mysql"]);
    }
} 
