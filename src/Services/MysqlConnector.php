<?php  namespace Acme\Services;

use PDO;

class MysqlConnector
{
    private static $_db;

    private static $config;

    public function __construct(array $config)
    {
        self::$config = $config;
    }

    public static function db()
    {
        if(self::$_db)
        {
            return self::$_db;
        } else {
           $conn = new PDO(self::$config["dsn"], self::$config["username"], self::$config["password"]);
           $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           return self::$_db = $conn;
        }

    }

    public function persist($table, array $data)
    {
        list($valuestring, $fieldstring) = $this->prepareFieldsAndValueStrings($data);
        $stmt = self::db()->prepare("INSERT INTO $table ($fieldstring) VALUES ($valuestring)");
        $stmt->execute($data);

    }

    private function prepareFieldsAndValueStrings(array $data)
    {
        $valuestring = implode(", ", array_keys($data));
        $keys = array_keys($data);
        array_walk($keys, [$this, "cleanStrings"]);
        $fieldstring = implode(", ", $keys);
        return array($valuestring, $fieldstring);
    }

    public function getByField($table, $field, $input)
    {
        $stmt = self::db()->prepare("SELECT $field FROM $table WHERE $field = :$field");
        $stmt->execute([":$field" => $input]);
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    public function getAllRecords($table)
    {
        $stmt = self::db()->prepare("SELECT * FROM $table");
        $stmt->execute();
        $stmt->rowCount();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function getTotalRows($table)
    {
        $stmt = self::db()->prepare("SELECT * FROM $table");
        $stmt->execute();
        return $stmt->rowCount();
    }

    private function cleanStrings(&$arr)
    {
        $arr = trim($arr, ":");
    }
}
