<?php  namespace Acme\Services;

/*
 * wannabe nonce =)
 * output is not an 100% guaranteed unique string, but for this purpose i thinkg it's ok. :)
 */
class NonceMaker
{

    /**
     * Secret salt, dont tell anyone.
     */
    private $salt = "galapagosislands is a nice place to visit when its cold in sweden";

    /**
     * Generate a nonce.
     * @return string
     */
    public function make()
    {
       $rand = $this->getRandomString();
       return hash("sha256", $this->salt . $rand . time());

    }

    /**
     * Randomize a string with specified lenght
     *
     * @param int $length
     * @return string
     */
    public function getRandomString($length = 64)
    {
        $string = "";
        for ($i = 0; $i < $length; $i++) {
            $string .= mt_rand(0, 9);
        }
        return $string;
    }
}
