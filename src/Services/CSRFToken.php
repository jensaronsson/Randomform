<?php  namespace Acme\Services;

use Exception;

/**
 * Generates and validates CSRFTokens
 */
class CSRFToken
{
    private $csrftoken = "_csrf_token";

    /**
     * @param NonceMaker $nonce
     */
    private $nonce;

    /**
     * @param NonceMaker $nonce
     */
    public function __construct(NonceMaker $nonce)
    {
        $this->nonce = $nonce;
    }

    /**
     * Generate a random token and store in session
     */
    public function generateToken()
    {
        $nonce = $this->nonce->make();
        $_SESSION[$this->csrftoken] = $nonce;
        return $nonce;
    }

    /**
     * Validate the token from form
     *
     * @param string $token
     *
     * @throws \Exception
     * @return Boolean
     */
    public function validateToken($token)
    {
        if (isset($_SESSION[$this->csrftoken])) {
            $sToken = $_SESSION[$this->csrftoken];
            unset($_SESSION[$this->csrftoken]);
            if ($sToken !== $token)
                throw new Exception("Token is invalid!");
            return true;
        }
    }
} 
