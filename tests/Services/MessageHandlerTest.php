<?php
require "src/Services/MessageHandler.php";
use Acme\Services\MessageHandler;

class MessageHandlerTest extends PHPUnit_Framework_TestCase {

    private $messageHandler;

    public function setUp()
    {
        $this->messageHandler = new MessageHandler();
    }

    public function testAddMessageToHandlerShouldAddOneToArray()
    {
       $this->messageHandler->add("error", "Test message");
       $this->assertCount(1, $this->messageHandler->all());
    }

    public function testAddMesssageToHandlerAndRemoveItShouldCountZeroInArray()
    {
        $this->messageHandler->add("error", "Test message");
        $this->assertCount(1, $this->messageHandler->all());
        $this->messageHandler->remove("error");
        $this->assertCount(0, $this->messageHandler->all());
    }

    public function testShouldBeEmpty()
    {
       $this->testAddMesssageToHandlerAndRemoveItShouldCountZeroInArray();
       $this->assertTrue($this->messageHandler->isEmpty() === true);
    }

}
