<?php
require "src/Services/CSRFToken.php";
require "src/Services/NonceMaker.php";

use Acme\Services\CSRFToken;
use Acme\Services\NonceMaker;

class CSRFTokenTest extends PHPUnit_Framework_TestCase {

    private $token;

    public function setUp()
    {
       $this->token = new CSRFToken(new NonceMaker());
    }

    public function testGenerateTokenShouldPlaceTokenInSession()
    {
        $token = $this->token->generateToken();
        $this->assertEquals($_SESSION["_csrf_token"], $token);
    }

    /**
     * @expectedException Exception
     */
    public function testThatValidatingAnInvalidTokenThrowsException()
    {
       $this->token->generateToken();
       $this->token->validateToken("invalidtoken");
    }

    public function testValidatingValidTokenReturnsTrueAndRemovesTokenFromSessionArray()
    {
      $token = $this->token->generateToken();
      $result = $this->token->validateToken($token);
      $this->assertTrue($result === true);
      $this->assertArrayNotHasKey("_csrf_token", $_SESSION);
    }
}
